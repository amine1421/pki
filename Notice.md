# Mise en oeuvre de l'environement

## Prérequis

- virtualbox : https://www.virtualbox.org/wiki/Downloads
- vagrant : https://developer.hashicorp.com/vagrant/downloads

## Installation

Cloner le dépot git : https://gitlab.com/ssr-conseil/pki

Depuis le dossier pki, lancer la commande suivante : vagrant up

Sur votre machine hôte, configurer en serveur dns secondaire 192.168.56.10

## Infrastructure

L'infrastructure comporte 3 serveurs : enregistré sur le domaine tssr.local

- dns : 192.168.56.10
- pki : 192.168.56.11
- web : 192.168.56.12

Les trois serveurs possèdent un compte admin (pasword : pass) avec les droits sudo 

### dns

Utilisé pour la résolution DNS de domaine sur l'environement

### pki

l'autorité de certification éxecute step-ca (https://smallstep.com/docs/step-ca), en écoute sur le port 8443.
En l'état le service propose un serveur ACME. 

Ce dernier est accessible via https://pki.tssr.local:8443/acme/acme/directory

La CA est configuré pour pouvoir emmetre des certificats SSL

Le certificat racine de cette CA est disponible dans /etc/step-ca/certs

### web

Le serveur web execute un serveur Apache2 configuré en http://web.tssr.local

## Exercice

**L'ensemble des actions que vous effectuerez devra être documenté, un rendu est attendu pour pouvoir vous évaluer sur le module de la semaine**

- Créer un compte gitlab (si vous n'en possédez pas déja un).
- Mettre en place l'authentification par clef sur ce compte.
- Rédiger la procédure permettant de mettre en place une connection ssh par par cle  (depuis votre PC d'admin et entre les serveurs)
- Configurer les 3 serveurs pour permettre une connexion en ssh par clé depuis votre machine hôte
- Générer des couples de clée pour les utilisateurs admin de chaque machines
- Configurer la connection par clef SSH entre tout les serveurs
- Documentez vous sur l'utilisation de certbot.
- Mettre en place SSL sur le serveur web à l'aide de Certbot (nous utiliserons notre CA, au lieu de let'sencrypt)
- mettre en place le renouvellement automatique du certificat.
- Tester la connection en https:// Que constatez vous ? Comment résoudre **proprement le problème rencontré**
- Identifier la différence entre SFTP et FTPS ?
**Bonnus**
- Mettre en place l'une des deux solutions précédentes pour permettre à l'utilisateur webmaster de déposer des fichiers dans /var/www/html




